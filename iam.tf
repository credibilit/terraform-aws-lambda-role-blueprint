resource "aws_iam_role" "lambda" {
  name = "${var.name}"
  assume_role_policy = "${file("${path.module}/roles/lambda.json")}"
}

resource "aws_iam_role_policy_attachment" "attach_basic_lambda_policy" {
  role       = "${aws_iam_role.lambda.name}"
  policy_arn = "${var.default_policy}"
}

resource "aws_iam_role_policy_attachment" "attach_extra_policies" {
  role       = "${aws_iam_role.lambda.name}"
  policy_arn = "${element(var.extra_iam_policies_arn, count.index)}"

  count = "${var.extra_iam_policies_count}"
}

output "role" {
  value = {
    name      = "${aws_iam_role.lambda.name}"
    arn       = "${aws_iam_role.lambda.arn}"
    unique_id = "${aws_iam_role.lambda.unique_id}"
    path      = "${aws_iam_role.lambda.path}"
    roles     = "${concat(
      list(var.default_policy),
      var.extra_iam_policies_arn
    )}"
  }
}
