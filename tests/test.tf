variable "account" { }

variable "name" {
  default = "aws-lambda-test-acme"
}

variable "deploy_directory" {
  default = "build"
}

variable "artifact" {
  default = "hello-world.zip"
}

module "lambda_role" {
  source                    = "../"
  name                      = "${var.name}"
  extra_iam_policies_arn    = [
    "${aws_iam_policy.publish_sns.arn}",
    "${aws_iam_policy.xray_put_segments.arn}"
  ]
  extra_iam_policies_count  = 2
}

output "role" {
  value = "${module.lambda_role.role}"
}
