terraform {
  required_version = ">=0.10.0"

  backend "s3" {
    bucket = "495770326048-tfstate"
    key    = "blueprints/tests/aws-lambda-acme.tfstate"
    region = "us-east-1"
  }
}
