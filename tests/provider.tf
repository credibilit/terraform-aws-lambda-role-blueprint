provider "aws" {
  version = "~> 0.1.4"
  
  allowed_account_ids = [
    "${var.account}"
  ]
}
