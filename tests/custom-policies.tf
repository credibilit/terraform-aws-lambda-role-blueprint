data "aws_iam_policy_document" "publish_sns" {
  statement {
    sid = "SendAlert"
    resources = ["*"]
    actions = ["sns:Publish"]
  }
}

resource "aws_iam_policy" "publish_sns" {
  name = "${var.name}-publish-sns"
  policy = "${data.aws_iam_policy_document.publish_sns.json}"
}

data "aws_iam_policy_document" "xray_put_segments" {
  statement {
    sid = "XRayIntegration"
    resources = ["*"]
    actions = ["xray:PutTraceSegments"]
  }
}

resource "aws_iam_policy" "xray_put_segments" {
  name = "${var.name}-xray-put-segments"
  policy = "${data.aws_iam_policy_document.xray_put_segments.json}"
}
