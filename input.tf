variable "name" {
  type = "string"
  description = "A label to name role"
}

variable "default_policy" {
  type = "string"
  default = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  description = "The default Lambda policy ARN to attach to the Lambda role. Only set if you really know what are doing"
}

variable "extra_iam_policies_arn" {
  type = "list"
  default = []
  description = "List of ARN of each custom policy to add to the Lambda role"
}

variable "extra_iam_policies_count" {
  type = "string"
  default = 0
  description = "Number of custom policies to add to the Lambda role"
}
